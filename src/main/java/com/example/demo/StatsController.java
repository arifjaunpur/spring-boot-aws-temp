package com.example.demo;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;


@RestController
public class StatsController {

  @Value("${GIT_COMMIT_HASH}")
  private String version;

  @RequestMapping("/")
  public String index() {
    return "use <a href='/version'>/version</a> to view git commit hash deployed";
  }
  
  @RequestMapping("/version")
  public String getVersion() {
    return version;
  }
}